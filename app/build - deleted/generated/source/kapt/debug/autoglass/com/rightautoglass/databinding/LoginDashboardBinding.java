package autoglass.com.rightautoglass.databinding;
import autoglass.com.rightautoglass.R;
import autoglass.com.rightautoglass.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LoginDashboardBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.image, 1);
        sViewsWithIds.put(R.id.btnCustomer, 2);
        sViewsWithIds.put(R.id.btnInstaller, 3);
        sViewsWithIds.put(R.id.txtpower, 4);
        sViewsWithIds.put(R.id.image_power, 5);
    }
    // views
    @NonNull
    public final android.widget.Button btnCustomer;
    @NonNull
    public final android.widget.Button btnInstaller;
    @NonNull
    public final android.widget.ImageView image;
    @NonNull
    public final android.widget.ImageView imagePower;
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    public final android.widget.TextView txtpower;
    // variables
    @Nullable
    private autoglass.com.rightautoglass.Home mHome;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LoginDashboardBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds);
        this.btnCustomer = (android.widget.Button) bindings[2];
        this.btnInstaller = (android.widget.Button) bindings[3];
        this.image = (android.widget.ImageView) bindings[1];
        this.imagePower = (android.widget.ImageView) bindings[5];
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.txtpower = (android.widget.TextView) bindings[4];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.home == variableId) {
            setHome((autoglass.com.rightautoglass.Home) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setHome(@Nullable autoglass.com.rightautoglass.Home Home) {
        this.mHome = Home;
    }
    @Nullable
    public autoglass.com.rightautoglass.Home getHome() {
        return mHome;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static LoginDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static LoginDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<LoginDashboardBinding>inflate(inflater, autoglass.com.rightautoglass.R.layout.login_dashboard, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static LoginDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static LoginDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(autoglass.com.rightautoglass.R.layout.login_dashboard, null, false), bindingComponent);
    }
    @NonNull
    public static LoginDashboardBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static LoginDashboardBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/login_dashboard_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new LoginDashboardBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): home
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}