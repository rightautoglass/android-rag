package autoglass.com.rightautoglass.databinding;
import autoglass.com.rightautoglass.R;
import autoglass.com.rightautoglass.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class InstallerBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.ln_one, 1);
        sViewsWithIds.put(R.id.btnYear, 2);
        sViewsWithIds.put(R.id.btnVIN, 3);
        sViewsWithIds.put(R.id.ln_two, 4);
        sViewsWithIds.put(R.id.btnPicture, 5);
        sViewsWithIds.put(R.id.btnPaydection, 6);
        sViewsWithIds.put(R.id.rl_bottom, 7);
        sViewsWithIds.put(R.id.btnChat, 8);
    }
    // views
    @NonNull
    public final android.widget.ImageView btnChat;
    @NonNull
    public final android.widget.Button btnPaydection;
    @NonNull
    public final android.widget.Button btnPicture;
    @NonNull
    public final android.widget.Button btnVIN;
    @NonNull
    public final android.widget.Button btnYear;
    @NonNull
    public final android.widget.LinearLayout lnOne;
    @NonNull
    public final android.widget.LinearLayout lnTwo;
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    public final android.widget.RelativeLayout rlBottom;
    // variables
    @Nullable
    private autoglass.com.rightautoglass.Installer mInstaller;
    @Nullable
    private autoglass.com.rightautoglass.Customer mCustomer;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public InstallerBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds);
        this.btnChat = (android.widget.ImageView) bindings[8];
        this.btnPaydection = (android.widget.Button) bindings[6];
        this.btnPicture = (android.widget.Button) bindings[5];
        this.btnVIN = (android.widget.Button) bindings[3];
        this.btnYear = (android.widget.Button) bindings[2];
        this.lnOne = (android.widget.LinearLayout) bindings[1];
        this.lnTwo = (android.widget.LinearLayout) bindings[4];
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.rlBottom = (android.widget.RelativeLayout) bindings[7];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.installer == variableId) {
            setInstaller((autoglass.com.rightautoglass.Installer) variable);
        }
        else if (BR.customer == variableId) {
            setCustomer((autoglass.com.rightautoglass.Customer) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setInstaller(@Nullable autoglass.com.rightautoglass.Installer Installer) {
        this.mInstaller = Installer;
    }
    @Nullable
    public autoglass.com.rightautoglass.Installer getInstaller() {
        return mInstaller;
    }
    public void setCustomer(@Nullable autoglass.com.rightautoglass.Customer Customer) {
        this.mCustomer = Customer;
    }
    @Nullable
    public autoglass.com.rightautoglass.Customer getCustomer() {
        return mCustomer;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static InstallerBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static InstallerBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<InstallerBinding>inflate(inflater, autoglass.com.rightautoglass.R.layout.installer, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static InstallerBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static InstallerBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(autoglass.com.rightautoglass.R.layout.installer, null, false), bindingComponent);
    }
    @NonNull
    public static InstallerBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static InstallerBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/installer_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new InstallerBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): installer
        flag 1 (0x2L): customer
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}