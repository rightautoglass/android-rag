
package android.databinding;
import autoglass.com.rightautoglass.BR;
class DataBinderMapper  {
    final static int TARGET_MIN_SDK = 23;
    public DataBinderMapper() {
    }
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case autoglass.com.rightautoglass.R.layout.installer:
                    return autoglass.com.rightautoglass.databinding.InstallerBinding.bind(view, bindingComponent);
                case autoglass.com.rightautoglass.R.layout.login_dashboard:
                    return autoglass.com.rightautoglass.databinding.LoginDashboardBinding.bind(view, bindingComponent);
        }
        return null;
    }
    android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case 1226992084: {
                if(tag.equals("layout/installer_0")) {
                    return autoglass.com.rightautoglass.R.layout.installer;
                }
                break;
            }
            case 550166986: {
                if(tag.equals("layout/login_dashboard_0")) {
                    return autoglass.com.rightautoglass.R.layout.login_dashboard;
                }
                break;
            }
        }
        return 0;
    }
    String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"
            ,"customer"
            ,"home"
            ,"installer"};
    }
}