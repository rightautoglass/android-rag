/**
 * Automatically generated file. DO NOT MODIFY
 */
package autoglass.com.rightautoglass;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "autoglass_app.com.rightautoglass";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 23;
  public static final String VERSION_NAME = "1.4.4";
}
