package autoglass.com.rightautoglass;

import org.json.JSONObject;

/**
 * Created by chrisvaughan on 3/30/18.
 */

public class EnvelopeData {
    private JSONObject envelope;
    private JSONObject recipients;
    private JSONObject documents;


    public EnvelopeData(JSONObject envelope){
        this.envelope = envelope;
    }

    public JSONObject getEnvelope(){
        return this.envelope;
    }

    public JSONObject getRecipients(){
        return this.recipients;
    }
    public JSONObject getDocuments(){
        return this.documents;
    }

    public void setDocuments(JSONObject documents){
        this.documents = documents;
    }

    public void setEnvelope(JSONObject envelope){
        this.envelope = envelope;
    }

    public void setRecipients(JSONObject recipients){
        this.recipients = recipients;
    }

}
