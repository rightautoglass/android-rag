package autoglass.com.rightautoglass;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import autoglass.com.rightautoglass.databinding.LoginDashboardBinding;

/**
 * Created by satnamsingh on 24/01/18.
 */

public class Home extends Activity implements View.OnClickListener {

    LoginDashboardBinding binding;
    Intent mIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.login_dashboard);
        binding.btnCustomer.setOnClickListener(this);
        binding.btnInstaller.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCustomer:
                mIntent = new Intent(Home.this, Customer.class);
                startActivity(mIntent);
                break;

            case R.id.btnInstaller:

                //mIntent = new Intent(Home.this, AuthenticatorActivity.class);
                mIntent = new Intent(Home.this, Installer.class);
                startActivity(mIntent);
                break;
            default:
                break;
        }
    }
}
