package autoglass.com.rightautoglass

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
//import com.amazonaws.mobile.client.AWSMobileClient
import io.intercom.android.sdk.Intercom

class Splash : Activity() {
    private var mSharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // AWSMobileClient.getInstance().initialize(this).execute();
        setContentView(R.layout.activity_splash)
        val timerThread = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(4000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {

                    val mIntent = Intent(this@Splash, MainLogin::class.java)
                    startActivity(mIntent)
                    overridePendingTransition(R.anim.right_in, R.anim.left_out)
                    finish()
                }
            }
        }

        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
        timerThread.start()
    }

}
