package autoglass.com.rightautoglass.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;

import java.net.HttpURLConnection;

import autoglass.com.rightautoglass.R;
import autoglass.com.rightautoglass.utils.Config;

public class DocuSignOAuthService extends IntentService {

    private static final String TAG = DocuSignOAuthService.class.getSimpleName();
    public static final String ACTION_ACCESS_TOKEN = TAG + ".ActionAccessToken";
    public static final String EXTRA_CODE = TAG + ".code";
    public static final String EXTRA_ACCESS_TOKEN = TAG + ".AccessToken";
    public static final String EXTRA_ACCESS_TOKEN_ERROR = TAG + ".AccessTokenError";

    public DocuSignOAuthService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // see https://docs.docusign.com/esign/guide/authentication/oa2_auth_code.html
        String code = intent.getStringExtra(EXTRA_CODE);

        HttpURLConnection httpURLConnection;

        try {
            /*String message = Config.PARAMETER_GRANT_TYPE +
                    "=" +
                    Config.PARAMETER_AUTHORIZATION_CODE +
                    "&" +
                    Config.PARAMETER_CODE +
                    "=" +
                    URLEncoder.encode(code, Config.UTF8) +
                    "&" +
                    Config.PARAMETER_REDIRECT_URI +
                    "=" +
                    Config.PARAMETER_CALLBACK;

            URL url = new URL(new URL(Config.OAUTH_BASE_URL), Config.FULL_OAUTH_TOKEN);

            OkHttpClient okHttpClient = new OkHttpClient();

            httpURLConnection = new OkUrlFactory(okHttpClient).open(url);
            // Add request properties
            String headerCode = Base64.encodeToString((Config.CLIENT_ID + ":" + Config.CLIENT_SECRET).getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
            httpURLConnection.addRequestProperty(Config.AUTHORIZATION, Config.BASIC + " " + headerCode.trim());
            httpURLConnection.addRequestProperty(Config.CONTENT_TYPE, Config.APPLICATION_X_FORM_URLENCODED);

            httpURLConnection.setRequestMethod(Config.HTTP_POST);

            httpURLConnection.setChunkedStreamingMode(0);
            httpURLConnection.setDoOutput(true);

            OutputStream os = httpURLConnection.getOutputStream();
            os.write(message.getBytes(Config.UTF8.toUpperCase()));
            os.close();*/

            //AccessTokenModel accessToken = new AccessTokenModel();  //httpURLConnection.getInputStream(), AccessTokenModel.class);
            String token = code; //accessToken.access_token;
            //String error = accessToken.error;


            //save the token for later use
            SharedPreferences sharedPreferences = getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("auth_key", token);
            editor.commit();

            Intent tokenIntent = new Intent(ACTION_ACCESS_TOKEN);
            tokenIntent.putExtra(EXTRA_ACCESS_TOKEN, token);
            tokenIntent.putExtra(EXTRA_ACCESS_TOKEN_ERROR, "not_used");
            LocalBroadcastManager.getInstance(this).sendBroadcast(tokenIntent);

        } catch (Exception e) {
            //Log.d(TAG, e.getMessage());
        }

    }

    public static String getLoginUrl() {
        Uri.Builder oauthBuilder = Uri.parse(Config.OAUTH_BASE_URL)
                .buildUpon()
                .path(Config.FULL_OAUTH_AUTH)
                .appendQueryParameter(Config.PARAMETER_CLIENT_ID, Config.CLIENT_ID)
                .appendQueryParameter(Config.PARAMETER_RESPONSE_TYPE, Config.PARAMETER_CODE)
                .appendQueryParameter(Config.PARAMETER_REDIRECT_URI, Config.PARAMETER_CALLBACK)
                .appendQueryParameter(Config.PARAMETER_SCOPE, Config.PARAMETER_ALL);

        Uri oauthUri = oauthBuilder.build();
        return oauthUri.toString();
    }






}
