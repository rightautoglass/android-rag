package autoglass.com.rightautoglass.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import autoglass.com.rightautoglass.EnvelopeData;
import autoglass.com.rightautoglass.R;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;

import static android.content.Context.MODE_PRIVATE;
import static utlity.Utility.isNetworkAvailable;

//import com.amazonaws.Response;


/**
 * Created by chrisvaughan on 3/22/18.
 */

public class CurlBasic {

    private String authToken;
    protected Context mContext;
    String token;
    String baseURI;
    String accountID;

    String urlInbox;
    String urlEnvelopes;
    String urlSignatures;
    String urlDeleteSignatures;
    String envelopeId;
    String documentId;
    String urlLoginInfo = "https://na2.docusign.net/restapi/v2/login_information";

    StringBuilder builder = new StringBuilder();

    //DefaultHttpClient client = new DefaultHttpClient();

    public CurlBasic(Context context){
        this.mContext = context.getApplicationContext();
    }

    public CurlBasic(Context context, String envelope_id){
        this.mContext = context.getApplicationContext();
        this.envelopeId = envelope_id;
    }

    public String getUser (){
        //make sure we have a token
        if(!verifyToken()){
            return null;
        }else{
            return "success";
        }
/*
        String res="";
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL("https://account.docusign.com/oauth/userinfo");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("X-DocuSign-Authentication", token);

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        JSONObject jObject =  null;
        JSONObject uObject = null;

        try {
            jObject = new JSONObject(res);
            uObject =jObject;
            JSONArray arr = jObject.getJSONArray("accounts");
            jObject = arr.getJSONObject(arr.length()-1);//get the last account.  this is a fringe case, if a user has multiple associated accounts.
        } catch (JSONException e) {
            e.printStackTrace();
           //Log.d("json error", e.getMessage());
            return null;
        }

        //todo: parse json object to get user id and and base url
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);

        String accountID = "";
        String baseURI = "";
        String uname = "";
        String uemail = "";
        try {
            accountID = jObject.getString("account_id");
            baseURI = jObject.getString("base_uri");
            uname = uObject.getString("name");
            uemail = uObject.getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
           //Log.d("curl","account id or base uri not found");

        }

       //Log.d("curl",accountID + "/" + baseURI);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("account_id", accountID);
        editor.putString("base_uri", baseURI);
        editor.putString("userName", uname);
        editor.putString("userEmail", uemail);
        editor.commit();


        return res; //builder.toString();*/
    }

    public String exchangeCode(String code) {

        String res="";
        HttpURLConnection urlConnection = null;
        try {


            URL url = new URL("https://account.docusign.com/oauth/token"); //get token
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            String hash = Config.CLIENT_ID + ":" + Config.CLIENT_SECRET;
            String encoded = "Basic " + Base64.encodeToString(hash.getBytes(), Base64.NO_WRAP );
           //Log.d("encoded", encoded);
            urlConnection.setRequestProperty("Authorization", encoded);

            JSONObject body = new JSONObject();
            body.put("grant_type","authorization_code");
            body.put("code",code);

            OutputStreamWriter osw = new OutputStreamWriter(urlConnection.getOutputStream());
            //Log.d("Curl", payload.toString().replace("\\",""));
            osw.write(String.format(body.toString()));

            osw.flush();
            //read the return data
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);

           //Log.d("--------post response", res);
            osw.close();

           //Log.d("curl", String.valueOf(urlConnection.getResponseCode()));


            if(urlConnection.getResponseCode() == 201 || urlConnection.getResponseCode() == 200){
                JSONObject jres = new JSONObject(res);
                //Log.d("----returnobj", jres.getString("url"));


                //String code = values.get(Config.PARAMETER_CODE);
                String token = jres.getString("access_token" );
                String rtoken = jres.getString("refresh_token" );
                Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
                calendar.add(Calendar.SECOND, Integer.parseInt(jres.getString("expires_in" )));

                String expires = String.valueOf(calendar.getTimeInMillis());
                //String error = jres.get(Config.PARAMETER_ERROR);
                //String errorDescription = values.get(Config.PARAMETER_ERROR_DETAILS);

                SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("auth_key", token);
                editor.putString("auth_key_expiration", expires);
                editor.putString("refresh_key", rtoken);
                editor.commit();


            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        ////Log.d("Curl",res.toString());

        return res;
    }


    /*verify that we have a token before we do any work with the api*/
    public boolean verifyToken(){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
        if(sharedPreferences != null  && sharedPreferences.getString("first_app_run_1","false").equals("true") ){
            String akey = sharedPreferences.getString("auth_key","no key available");
            //String ekey = sharedPreferences.getString("auth_key_expiration","no expiration available");
           //Log.d("existing token",akey);
            //Calendar calendar = Calendar.getInstance();

            if(akey != "no key available" /*&& ekey != "no expiration available" && TextUtils.isDigitsOnly(ekey) && (calendar.getTimeInMillis() < Long.valueOf(ekey)) */){
                token = akey;
                return true;
            }
        }else{
           //Log.d("existing token","not token found");
            // clear token data
          /*  SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("account_id",null);
            editor.putString("base_uri", null);
            editor.putString("auth_key", null);
            editor.putString("auth_key_expires", null);
            editor.commit();*/
            if( sharedPreferences.getString("first_app_run_1","false").equals("false") ){
                //CustomApplication.getInstance().clearApplicationData();
                logoutUser();
            }else {
                logoutUser();
            }

        }

        return false;
    }
    /*verify that we have user info*/
    public boolean verifyUserInfo(){
        boolean ret = false;
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
        if(sharedPreferences != null){
            baseURI = sharedPreferences.getString("base_uri","no uri");
           //Log.d("existing uri",baseURI);
            if(baseURI != "no uri"){
                ret = true;
            }
            accountID = sharedPreferences.getString("account_id","no account id");
           //Log.d("existing account id",accountID);
            if(accountID != "no account id"){
                ret = true;
            }
        }else{
           //Log.d("existing uri","not token found");
            logoutUser();
            ret = false;
        }

        return ret;
    }

    public static String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }

    public static String readByteStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }
    public boolean generateURLS(){

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
        String user_id = sharedPreferences.getString("userId","nouid");

        urlInbox = baseURI +  "/folders/inbox";
        urlEnvelopes = baseURI + "/envelopes";
        urlSignatures = baseURI + "/users/"+user_id+"/signatures/";// for getting signatures
        urlDeleteSignatures = baseURI + "/captive_recipients/signature";// for deleting signatures

        return true;
    }

    public String getEnvelopeURL(String envelope_id){
        generateURLS();
        String res = urlEnvelopes + "/" + envelope_id;
        return res;
    }

    public HashMap getContracts() {
        //make sure we have a token
        if(!verifyToken() || !verifyUserInfo()){
            return null;
        }

        String res="";
        HashMap<String,String> ret=null;
        HttpURLConnection urlConnection = null;
        try {
            generateURLS();
            URL url = new URL(urlInbox); //get the inbox
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("X-DocuSign-Authentication",  token);

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);

            //res should now be the inbox object.
            //get the folder items array if the resultSetSize is more than 0
            JSONObject jObject =  null;
            JSONArray arr;

            try {
                jObject = new JSONObject(res);
                Integer numItems = jObject.getInt("resultSetSize");
                if(numItems>0){
                    arr = jObject.getJSONArray("folderItems");//get the folder items array
                    //step through this list and return the values to PendingContracts.java
                   //Log.d("Curl",arr.toString());
                    HashMap<String, String> inboxItems = new HashMap<String,String>();
                    for(int i=0; i<arr.length(); i++){
                       //Log.d("status-----",arr.getJSONObject(i).getString("status"));
                        String status = arr.getJSONObject(i).getString("status");
                        if(arr.getJSONObject(i).has("completedDateTime") == false &&  !status.trim().equals("voided".trim()) ){ //ignore completed items
                            String value = arr.getJSONObject(i).getString("envelopeId");
                            String name = arr.getJSONObject(i).getString("subject");
                            inboxItems.put(name, value);
                        }
                    }

                    ret = inboxItems;
                }else{
                    ret =  new HashMap<String,String>();
                }

            } catch (JSONException e) {
                //e.printStackTrace();
                return null;
            }

        } catch (MalformedURLException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

       //Log.d("Curl",res.toString());

        return ret;
    }

    public EnvelopeData getEnvelope(String envelopeId) {
        //make sure we have a token
        if(!verifyToken() || !verifyUserInfo()){
            return null;
        }

        EnvelopeData envelope = null;
        String res="";
        EnvelopeData ret=null;
        HttpURLConnection urlConnection = null;

        try {
            generateURLS();

            //get the envelope
            URL url = new URL(urlEnvelopes+"/"+envelopeId); //get the envelope
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("X-DocuSign-Authentication",token);

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);

            JSONObject jObject =  null;
            try {
                jObject = new JSONObject(res);
                envelope = new EnvelopeData(jObject);

            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }

            //Get the recipients
            url = new URL(urlEnvelopes+"/"+envelopeId+"/recipients"); //get the recipients
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("X-DocuSign-Authentication", token);

            in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);

            try {
                jObject = new JSONObject(res);
                envelope.setRecipients(jObject);

            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }

            //Get the documents
            url = new URL(urlEnvelopes+"/"+envelopeId+"/documents"); //get the recipients
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("X-DocuSign-Authentication",token);

            in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);

            try {
                jObject = new JSONObject(res);
                envelope.setDocuments(jObject);

            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            ret = envelope;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

       //Log.d("Curl",res.toString());

        return ret;
    }


    public byte[] getEnvelopeDocument(String docId, String s) throws Exception {
        if(!verifyToken() || !verifyUserInfo()){
            return null;
        }

        HttpURLConnection urlConnection = null;
        String errorMessage = null;
        byte[] documentFile;

        try {
            generateURLS();
            URL url = new URL(urlEnvelopes+"/"+envelopeId+"/documents/"+documentId); //get the document

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("X-DocuSign-Authentication", token);

            InputStream in = urlConnection.getInputStream();

            byte[] buffer = new byte[4096];
            int n;

            ByteArrayOutputStream output = null;
            while ((n = in.read(buffer)) != -1)
            {
                output.write(buffer, 0, n);
            }
            output.close();

            documentFile = output.toByteArray();
           /* Response<ResponseBody> response = envelopesApi.getDocument(accountId, documentId, envelopeId, null, null, null, null, null, null, null).execute();
            documentFile = response.body().bytes();
            if (response.errorBody() != null)
                errorMessage = response.errorBody().string();
            if (errorMessage != null) {
                throw new Exception("Error while calling DocuSign API: " + errorMessage);
            }*/

        } catch (Exception ex) {
            throw new Exception("Error while calling EnvelopesHelper getEnvelopeDocument: " + ex.getMessage());
        }
        return documentFile;
    }

    public boolean hasInternetAccess() {
        if (isNetworkAvailable(mContext)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection)
                        (new URL("http://clients3.google.com/generate_204")
                                .openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 204 &&
                        urlc.getContentLength() == 0);
            } catch (IOException e) {
                //Log.e(TAG, "Error checking internet connection", e);
            }
        } else {
        }
        return false;
    }

    public JSONObject signerInfo(String s, EnvelopeData envelope) throws JSONException {
        //create the json object to be submitted
        //we're mostly just resubmitting the same signer info, except we're adding a clientuserid
        //for embedded signing.
        JSONObject ret = new JSONObject();
        Random random = new Random();
        String clientUserId = "";//String.valueOf(random.nextInt(500) + random.nextInt(400) + random.nextInt(1000));
       //Log.d("envelopeobj",envelope.getRecipients().getJSONArray("signers").getJSONObject(0).toString());
        String email= envelope.getRecipients().getJSONArray("signers").getJSONObject(0).getString("email"); //clientUserId + s.replace(" ","_")+"@rightauto.glass";
        String recipientId = envelope.getRecipients().getJSONArray("signers").getJSONObject(0).getString("recipientId");
        String userId = envelope.getRecipients().getJSONArray("signers").getJSONObject(0).getString("userId");
        String name = envelope.getRecipients().getJSONArray("signers").getJSONObject(0).getString("name");
        String guid = envelope.getRecipients().getJSONArray("signers").getJSONObject(0).getString("recipientIdGuid");
        try {
            //ret.put("userName", s);
            //ret.put("name", name);
            ret.put("email", email);
            ret.put("userId",userId);
            ret.put("recipientId", recipientId);
            ret.put("clientUserId", clientUserId);
            ret.put("authenticationMethod", "email");
            ret.put("returnUrl", Config.PARAMETER_CALLBACK);

/*            ret.put("canSignOffline", "true");
            //ret.put("recipientIdGuid", guid);
            ret.put("requireIdLookup", "false");
            ret.put("routingOrder", "1");
            //ret.put("note", "signing initiated through app");*/



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public HashMap<String,String> getSigningInfo(String customerName, String envelopeId, EnvelopeData envelope) {
        //make sure we have a token
        if(!verifyToken() || !verifyUserInfo()){
            return null;
        }

        generateURLS();
        //delete previous signature
        if(deleteSignatures() == false){
            return null;
        }

        String res="";
        HashMap<String,String> ret= new HashMap<String,String>();
        HttpURLConnection urlConnection = null;

        try {

            Random random = new Random();
/*            URL url = new URL(urlEnvelopes+"/"+envelopeId+"/recipients"); //update recipient
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("PUT");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Authorization", "Bearer " + token);
            JSONArray signer = new JSONArray();*/
            JSONObject signerbody =  signerInfo(customerName, envelope);
 /*           signer.put(0,signerbody);
            JSONObject payload =  new JSONObject();
            payload.put("signers",signer);

            OutputStreamWriter osw = new OutputStreamWriter(urlConnection.getOutputStream());
           //Log.d("Curl", payload.toString().replace("\\",""));
            osw.write(String.format(payload.toString().replace("\\","")));

            osw.flush();

            //read the return data
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);

           //Log.d("--------put response", res);

            osw.close();

           //Log.d("curl", String.valueOf(urlConnection.getResponseCode()));
           //Log.d("Curl", String.valueOf(urlConnection.getResponseMessage()));
            if(urlConnection.getResponseCode() == 200) {//success*/

            URL url = new URL(urlEnvelopes+"/"+envelopeId+"/views/recipient"); //get signer url
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("X-DocuSign-Authentication",  token);
            OutputStreamWriter osw = new OutputStreamWriter(urlConnection.getOutputStream());
            //Log.d("Curl", payload.toString().replace("\\",""));
            osw.write(String.format(signerbody.toString().replace("\\","")));

            osw.flush();
            //read the return data
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            res = readStream(in);

           //Log.d("--------post response", res);
            osw.close();

           //Log.d("curl", String.valueOf(urlConnection.getResponseCode()));
            if(urlConnection.getResponseCode() == 201){
                JSONObject jres = new JSONObject(res);
               //Log.d("----returnobj", jres.getString("url"));

                ret.put("url",jres.getString("url"));
                if(signerbody.has("name")){
                    ret.put("name",signerbody.getString("name"));
                }else{
                    signerbody.getString("email");
                }

                ret.put("email",signerbody.getString("email"));

            }
          /*  }*/

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

       //Log.d("Curl",res.toString());

        return ret;
    }

    private boolean deleteSignatures(){
        Boolean ret = false;

        //make sure we have a token
        if(!verifyToken() || !verifyUserInfo()){
            return false;
        }

        String res = "";
        HttpURLConnection urlConnection = null;
        //https://NA3.docusign.net/restapi/v2/accounts/22459431/users/99a47846-8313-436d-aabb-a565f781e129/signatures/


        try {
            //get signatures
            URL url = new URL(urlSignatures); //get signatures

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("X-DocuSign-Authentication", token);

            //Log.d("auth------",auth.toString());
            //read the return data
            InputStream in = urlConnection.getInputStream();
            res = readStream(in);

            //Log.d("--------post response", res);

            JSONArray jarr = new JSONObject(res).getJSONArray("userSignatures");
            JSONObject jobj = null;
            if(jarr.length()>0){
                jobj = jarr.getJSONObject(0);
            }else{
                return true;
            }

            if(jobj == null){
                //no signatures, exit
                return true;
            }

            //Log.d("-----signatures",jobj.toString());

            String siguri = null;

            if(jobj.has("signatureImageUri")) {
                 siguri = jobj.getString("signatureImageUri");
            }else{//no image so we can exit
                return true;
            }

            //delete signatures
            url = new URL(baseURI + siguri);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestMethod("DELETE");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("X-DocuSign-Authentication", token);
            in = urlConnection.getInputStream();
            res = readStream(in);
            //Log.d("--------post response", res);

            //if we make it this far we're good
            return true;

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;

    }
    public Boolean loginUser(String User, String Pass) {


        String mKey = Config.CLIENT_ID;
        String res = "";

        if(verifyToken()){
            //if there's a token, we've done this already
            return true;
        }

        JSONObject auth = new JSONObject();


        HttpURLConnection urlConnection = null;
        try {
            auth.put("Username",User);
            auth.put("Password",Pass);
            auth.put("IntegratorKey",mKey);

            URL url = new URL(urlLoginInfo); //get user's accounts
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("X-DocuSign-Authentication", auth.toString());

            //Log.d("auth------",auth.toString());
            //read the return data
            InputStream in = urlConnection.getInputStream();
            res = readStream(in);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            //Log.d("Error Message: ", e.getMessage().toString());
            e.printStackTrace();
            //assume login failed
            return false;

        }


        JSONObject jObject =  null;

        JSONObject uObject = null;

        try {
            jObject = new JSONObject(res);
            uObject =jObject;
            JSONArray arr = jObject.getJSONArray("loginAccounts");
            jObject = arr.getJSONObject(arr.length()-1);//get the last account.  this is a fringe case, if a user has multiple associated accounts.
        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d("json error", e.getMessage());
            return null;
        }

        if(res == "") {//failed
            return false;
        }


        SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);

        String accountID = "";
        String baseURI = "";
        String uname = "";
        String uemail = "";
        String userId = "";
        try {
            accountID = jObject.getString("accountId");
            userId = jObject.getString("userId");
            baseURI = jObject.getString("baseUrl");
            uname = jObject.getString("userName");
            uemail = jObject.getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d("curl","account id or base uri not found");

        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("account_id", accountID);
        editor.putString("base_uri", baseURI);
        editor.putString("userName", uname);
        editor.putString("userEmail", uemail);
        editor.putString("userId", userId);
        editor.putString("auth_key", auth.toString());//replaces the oauth token
        editor.putString("first_app_run_1", "true");

        editor.commit();
        //initIntercomUser();//login to intercom


        return true;

    }

    public Boolean logoutUser() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("account_id",null);
        editor.putString("base_uri", null);
        editor.putString("auth_key", null);
        editor.putString("auth_key_expires", null);
        editor.putString("userName", null);
        editor.putString("userEmail", null);
        editor.putString("userId", null);
        editor.commit();
        //logout of intercom also
        //Intercom.client().logout();
        return true;
    }


    private void initIntercomUser(){

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
        if(sharedPreferences != null) {
            String username = sharedPreferences.getString("userName", "");
            String email = sharedPreferences.getString("userEmail", "");
            String acct = sharedPreferences.getString("account_id", "");
            UserAttributes userAttributes = new UserAttributes.Builder()
                    .withName(username.replace(" ","_"))
                    .withEmail(email)
                    .build();
            //Registration registration = Registration.create().withUserAttributes(userAttributes);

            Intercom.client().registerIdentifiedUser(new Registration().create().withUserAttributes(userAttributes));

            Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
/*
            String uhash = getHash(email);
            Intercom.client().setUserHash(uhash);*/
        }
    }
}