package autoglass.com.rightautoglass.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateDeserializer {

    public static final DateDeserializer INSTANCE = new DateDeserializer();

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    private static final SimpleDateFormat DATE_FORMAT_VIEW = new SimpleDateFormat("MM/dd/yyyy", Locale.US);

    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private DateDeserializer() {
    }

    public static String format(Date date) {
        return DATE_FORMAT_VIEW.format(date);
    }


}

