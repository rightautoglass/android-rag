package autoglass.com.rightautoglass;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
//import android.support.annotation.RequiresApi;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.docusign.esign.model.LoginAccount;
import autoglass.com.rightautoglass.service.DocuSignOAuthService;
import autoglass.com.rightautoglass.utils.Config;
import autoglass.com.rightautoglass.utils.CurlBasic;
import io.intercom.android.sdk.Intercom;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int LOAD_LOGIN_ACCOUNT = 1;
    Intent mIntent;

    private final BroadcastReceiver mAccessTokenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Retrieve AccessToken and call helper method to get user account information
            String accessToken = intent.getStringExtra(DocuSignOAuthService.EXTRA_ACCESS_TOKEN);
            String accessTokenError = intent.getStringExtra(DocuSignOAuthService.EXTRA_ACCESS_TOKEN_ERROR);

           //Log.d("access token",accessToken);
           //Log.d("access token error",accessTokenError);

            //retrieveLoginAccount(accessToken, accessTokenError);
        }
    };
    private WebView mWebView;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //check if we have a token already
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
        if(sharedPreferences != null ){
            String akey = sharedPreferences.getString("auth_key","no key available");
            String ekey = sharedPreferences.getString("auth_key_expiration","no expiration available");
           //Log.d("existing token",akey);
            Calendar calendar = Calendar.getInstance();

            if(akey != "no key available" && ekey != "no expiration available" && TextUtils.isDigitsOnly(ekey) && (calendar.getTimeInMillis() < Long.valueOf(ekey))){
                //if we have a key, fire the intent to load a new activity
                mIntent = new Intent(LoginActivity.this, PendingContracts.class);
                startActivity(mIntent);
            }
        }else{
           //Log.d("existing token","not token found");
        }

        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        mWebView = (WebView) findViewById(R.id.login_webView);


        setUpWebView();
    }

    public void onResume() {
        super.onResume();

        IntentFilter accessTokenFilter = new IntentFilter(DocuSignOAuthService.ACTION_ACCESS_TOKEN);
        LocalBroadcastManager.getInstance(this).registerReceiver(mAccessTokenReceiver, accessTokenFilter);
    }

    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mAccessTokenReceiver);
    }

    /**
     * Sets up the webview with DocuSign OAuth UI, in order to get an access token.
     */
    @SuppressWarnings("SetJavaScriptEnabled")
    private void setUpWebView() {
        mWebView.setWebViewClient(new DSOAuthWebViewClient());

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.2; nl-nl; Desire_A8181 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
        mWebView.getSettings().setSaveFormData(false);
        mWebView.getSettings().setSavePassword(false);

        mWebView.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);

        CookieSyncManager.createInstance(this);
        clearCookies();

        String url = DocuSignOAuthService.getLoginUrl();
        // load the url in the web view
        mWebView.loadUrl(url);
    }

    /**
     * Calls the Authentication Helper method to get user account information
     * @param accessToken the DocuSign OAuth token to use to make the API call.
     * @param error The error message, if any
     */
/*    private void retrieveLoginAccount(String accessToken, String error) {
        if (error != null) {
            handleError("", error);
        } else {
            SampleApp.getInstance().setAccessToken(accessToken);

            // Call helper function to retrieve User information
            //loginLoader();
        }
    }*/

    /**
     * starts the Send-or-Sign activity using the selected DocuSign login account
     * @param //loginAccount the DocuSign login account to be used for the rest of the app pages
     */
/*    public void displayLoginSignOrSendActivity(final LoginAccount loginAccount) {
        // store loginAccount to be shared across the application
        SampleApp.getInstance().setLoginAccount(loginAccount);

        // Open SignOrSendActivity
        startActivity(new Intent(this, SignOrSendActivity.class));
        finish();
    }*/

    protected void clearCookies() {
        CookieManager cookieManager = CookieManager.getInstance();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies(null);
        } else {
            cookieManager.removeAllCookie();
        }
    }

    /* get the current pending contracts */
    private class exchangeCode extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            CurlBasic curl = new CurlBasic(getApplicationContext());
            String res = curl.exchangeCode(params[0]);//in theory this is the code.
           //Log.d(TAG,res.toString());
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
           //Log.d(TAG,result.toString());
            //updateList(result);//send the mapped data to update the list


            mIntent = new Intent(LoginActivity.this, PendingContracts.class);
            startActivity(mIntent);mIntent = new Intent(LoginActivity.this, PendingContracts.class);
            startActivity(mIntent);


        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }


    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url != null && url.startsWith(Config.PARAMETER_CALLBACK)) {
            // handle DocuSign Oauth callback (after the user puts in the email and password)

            // parse the callback uri query parameters
            String data = Uri.parse(url).getQuery();
            if (data != null) {
                data = data.replace("code=","");
                new exchangeCode().execute(data);
            }

            /*
            HashMap<String, String> values = new HashMap<>();
            String[] parts = data.split("&");
            for (String part : parts) {
                String[] pieces = part.split("=");
                values.put(pieces[0], pieces.length == 1 ? "" : pieces[1]);
            }*/
            /*
            //String code = values.get(Config.PARAMETER_CODE);
            String token = values.get("access_token" *//*Config.PARAMETER_CODE*//*);
            Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
            calendar.add(Calendar.SECOND, Integer.parseInt(values.get("expires_in" *//*Config.PARAMETER_CODE*//*)));

            String expires = String.valueOf(calendar.getTimeInMillis());
            String error = values.get(Config.PARAMETER_ERROR);
            String errorDescription = values.get(Config.PARAMETER_ERROR_DETAILS);

            if (error != null) {
                handleError(error, errorDescription);
            } else if (token != null) {

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("auth_key", token);
                editor.putString("auth_key_expiration", expires);
                editor.commit();

                handleCode(token);
            } else {
                handleError("AccessToken Error", "Network Error");
            }
*/
            return true;
        } /*else if (url != null && url.contains(Config.PARAMETER_CHANGE_PASSWORD)) {
            // Handle DocuSign Oauth change-password
            //TODO Change Password
        } else {
            // Handle DocuSign Oauth forgot-password
            // TODO Forgot password
        }*/

        if (url != null && url.startsWith("http://")) {
            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return true;
        } else {
            return false;
        }
    }

    private void handleCode(String authCode) {
        // Call AccessToken API
        // Update authCode is now the actual token.
        /*Intent intent = new Intent(this, DocuSignOAuthService.class);
        intent.putExtra(DocuSignOAuthService.EXTRA_CODE, authCode);
        startService(intent);*/
        mIntent = new Intent(LoginActivity.this, PendingContracts.class);
        startActivity(mIntent);mIntent = new Intent(LoginActivity.this, PendingContracts.class);
        startActivity(mIntent);
    }

    private void handleError(String error, String errorDescription) {
        Toast.makeText(LoginActivity.this, error + "\n" + errorDescription, Toast.LENGTH_LONG).show();
    }

   /* private void loginLoader() {
        getSupportLoaderManager().initLoader(LOAD_LOGIN_ACCOUNT, null,
                new LoaderManager.LoaderCallbacks<List<LoginAccount>>() {

                    @Override
                    public Loader<List<LoginAccount>> onCreateLoader(int id, Bundle args) {
                        String accessToken = SampleApp.getInstance().getAccessToken();
                        // get the list of DocuSign accounts using an AsyncTaskLoader
                        return new LoginAccountLoader(LoginActivity.this, accessToken);
                    }

                    @Override
                    public void onLoadFinished(Loader<List<LoginAccount>> loader, List<LoginAccount> loginAccounts) {
                        hideLoading();
                        if (loginAccounts != null && loginAccounts.size() > 0) {
                            // a user can have multiple accounts. in that case we just get the first in the list
                            displayLoginSignOrSendActivity(loginAccounts.get(0));
                        } else {
                            handleError("this use is not part of any DocuSign acounts. Please contact your admin.", "");
                        }
                    }

                    @Override
                    public void onLoaderReset(Loader<List<LoginAccount>> loader) {
                        // no op //
                    }
                }).forceLoad();
    }
*/
/*    static class LoginAccountLoader extends AsyncTaskLoader<List<LoginAccount>> {
        private String token;

        private LoginAccountLoader(Context context, String accessToken) {
            super(context);
            token = accessToken;
        }

        @Override
        public List<LoginAccount> loadInBackground() {
            List<LoginAccount> accounts = new ArrayList<>();
            try {
                // call the DocuSign login API through the helper method
                accounts = SampleApp.getInstance().getAuthHelper().loginWithOauthToken(token);
            } catch (Exception e) {
               //Log.d(TAG, e.getMessage());
            }
            return accounts;
        }
    }*/

    /**
     * The web view for DocuSign OAuth flow
     */

    private class DSOAuthWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showLoading(getString(R.string.loading));
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            hideLoading();
            /** on Android N, if request is not called, Global login is not rendered for some reason
             * (i.e. if compiled on api 24 & run on a Android N device) **/
            view.requestLayout();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return LoginActivity.this.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            hideLoading();
            try {
                handleError(getString(R.string.webView_page_load_error), description);
                if (errorCode == ERROR_HOST_LOOKUP || errorCode == ERROR_BAD_URL) {
                    handleError("", description);
                }
            } catch (Exception e) {
                handleError(getString(R.string.webView_page_load_error), e.getMessage());
            }
        }
    }

    private void showLoading(String message) {
        if (mProgressDialog == null || !mProgressDialog.isShowing()) {
            mProgressDialog = ProgressDialog.show(this, null, message);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
        }
    }

    private void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()
                && !isFinishing()) {
            mProgressDialog.dismiss();
        }
    }

}