package autoglass.com.rightautoglass;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import io.intercom.android.sdk.Intercom;

/**
 * Created by Satnam on 1/30/2018.
 */

public class CustomApplication extends MultiDexApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override public void onCreate() {
        super.onCreate();
        //Intercom.initialize(this, "dG9rOmMzNmU4Y2JlX2I1YjFfNGI5MV85MTExX2ZmY2Y4YjU5YjUxYzoxOjA=", "oy4bejay");
        Intercom.initialize(this, "android_sdk-44233a130164bee6aec2deab30a92999885afe39", "oy4bejay");
        //Intercom.client().registerUnidentifiedUser();
        Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
    }
}
