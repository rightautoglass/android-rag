package autoglass.com.rightautoglass;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.bouncycastle.asn1.cms.EnvelopedData;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import autoglass.com.rightautoglass.utils.CurlBasic;
import io.intercom.android.sdk.Intercom;

public class Envelope extends AppCompatActivity{
    private String TAG = "Envelope";

    private String envelopeID;
    private TextView lblDocumentName;
    private Button lblCancel;
    private Button lblSign;
    private String customerName;
    private EnvelopeData envelope_data;
    private ListView lv;
    private List<String> mainList = new ArrayList<String>();

    CurlBasic curl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envelope);
        Toolbar toolbar = findViewById(R.id.toolbar);
        lblDocumentName = findViewById(R.id.lblDocumentName);
        lblCancel = findViewById(R.id.lblCancel);
        lblSign = findViewById(R.id.lblSign);
        lv = (ListView) findViewById(R.id.lstRecipients);

        lblSign.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.lblSign:
                        //Log.d(TAG,"sign button clicked");
                        signDoc();
                        break;
                    default:
                        break;

                }
            }
        });
        lblCancel.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.lblCancel:
                        onBackPressed();
                        break;
                    default:
                        break;

                }
            }
        });
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        //get the passed info
        this.envelopeID = getIntent().getStringExtra("EnvelopeID");

        //load the envelope data
        new loadEnvelope().execute();

    }

    protected void onResume() {

        super.onResume();

        new loadEnvelope().execute();//reload the envelopes list.
    }
    private class loadEnvelope extends AsyncTask<String, Void, EnvelopeData> {

        @Override
        protected EnvelopeData doInBackground(String... params) {

            curl = new CurlBasic(getApplicationContext());
            EnvelopeData res = curl.getEnvelope(envelopeID);
            //Log.d(TAG,res.getEnvelope().toString());
            return res;
        }

        @Override
        protected void onPostExecute(EnvelopeData result) {
            //Log.d(TAG,result.getRecipients().toString());
            envelope_data = result;
            updateView(result);
            /*try {
                JSONObject jObject = new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private void updateView(EnvelopeData envelope){
        try {
            lblDocumentName.setText(envelope.getEnvelope().getString("emailSubject"));
            customerName = envelope.getEnvelope().getString("emailSubject");

            //update the recipients
            JSONObject recipients = envelope.getRecipients().getJSONArray("signers").getJSONObject(0);
            //create a new array list
            List<String> array_list = new ArrayList<String>();

            //populate the list
                String value=customerName;

                array_list.add(value);

            //assign thelist to the adapter
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1,
                    array_list );

            //save the list for resume activity
            mainList = array_list;

            //update the list
            lv.setAdapter(arrayAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void signDoc(){
        new getSingingInfo().execute();
    }

    private class getSingingInfo extends AsyncTask<String, Void, HashMap<String,String>> {

        @Override
        protected HashMap<String, String> doInBackground(String... params) {
            curl = new CurlBasic(getApplicationContext());
            HashMap<String, String> res = curl.getSigningInfo(customerName, envelopeID, envelope_data);
            //Log.d(TAG,res.toString());

            Intent mIntent = new Intent(Envelope.this, EmbeddedSigningActivity.class);
            mIntent.putExtra(EmbeddedSigningActivity.SIGNING_URL, res.get("url"));
            mIntent.putExtra(EmbeddedSigningActivity.RECIPIENT_EMAIL, res.get("email"));

            startActivity(mIntent);
            return res;
        }

        @Override
        protected void onPostExecute(HashMap result) {
            //Log.d(TAG,result.toString());

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
