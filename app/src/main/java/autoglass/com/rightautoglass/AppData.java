package autoglass.com.rightautoglass;

import android.support.multidex.MultiDexApplication;

//import com.docusign.esign.helper.AuthenticationHelper;
import com.docusign.esign.model.LoginAccount;
import autoglass.com.rightautoglass.utils.Config;

/**
 * Application class
 */

public class AppData extends MultiDexApplication {

    private static final String TAG = AppData.class.getSimpleName();

    /**
     * Application object
     */
    private static AppData sApp;
    /**
     * AccessToken for authentication
     */
    private String mAccessToken;
    /**
     * LoginAccount for account info
     */
    private LoginAccount mAccount;
    private String mLastEnvelopeId;

    //private AuthenticationHelper sAuthHelper;

    public static AppData getInstance() {
        return sApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApp = this;
    }

/*    public AuthenticationHelper getAuthHelper() {
        if (sAuthHelper == null) {
            sAuthHelper = new AuthenticationHelper(Config.DS_BASE_URL ,Config.CLIENT_ID);
        }
        return sAuthHelper;
    }*/

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public LoginAccount getLoginAccount() {
        return mAccount;
    }

    public void setLoginAccount(LoginAccount account) {
        mAccount = account;
    }

    public String getLastEnvelopeId() {
        return mLastEnvelopeId;
    }

    public void setLastEnvelopeId(String lastEnvelopeId) {
        mLastEnvelopeId = lastEnvelopeId;
    }

}



