package autoglass.com.rightautoglass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import autoglass.com.rightautoglass.utils.CurlBasic;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;

//import com.amazonaws.util.StringUtils;

public class PendingContracts extends AppCompatActivity {
    private String TAG = "PendingContracts";
    private ListView lv;
    private ArrayList<String> envIds = new ArrayList<String>();
    private List<String> mainList = new ArrayList<String>();
    private int currentPage = 1;

    ImageButton ib;
    ImageButton pic;
    ImageButton ch;

    TextView numChats;

    CurlBasic curl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_contracts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.lstContracts);

        // Instanciating an array list (you don't need to do this,
        // you already have yours).
        List<String> your_array_list = new ArrayList<String>();

        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                your_array_list );

        lv.setAdapter(arrayAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                envelopeID = envIds.get(position);
                new loadEnvelope().execute();

            }
        });



        //Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
        //Intercom.client().setBottomPadding(20);


        //logout button
        TextView logout = findViewById(R.id.lblLogout);
        logout.setOnClickListener( new View.OnClickListener() {
            @Override public void onClick(View v) {
                new logout().execute();
            }
        });


        //bottom menu
        ib  = (ImageButton) findViewById(R.id.btnInbox);
        pic  = (ImageButton) findViewById(R.id.btnImages);
        ch  = (ImageButton) findViewById(R.id.btnChat);
        currentPage = 1;
        numChats = (TextView) findViewById(R.id.numChats);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchPage(1);
            }
        });
        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchPage(2);
                Intent mIntent = new Intent(PendingContracts.this, getPictures.class);
                startActivity(mIntent);
            }
        });

        ch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchPage(3);
                Intercom.client().displayMessenger();
                numChats.setVisibility(View.INVISIBLE);
                numChats.setText("0");
            }
        });



        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
        Intercom.client().setInAppMessageVisibility(Intercom.Visibility.GONE);
        Intercom.client().getUnreadConversationCount();
        switchPage(1);
        //moved lookup to asynctask
        new LongOperation().execute("");
        initChecks();

    }

    protected void initChecks(){
        final Handler handler = new Handler();
        Timer    timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                            try {
                                Integer count  = Intercom.client().getUnreadConversationCount();
                                if(count<1){
                                    numChats.setVisibility(View.INVISIBLE);
                                }else{
                                    numChats.setVisibility(View.VISIBLE);
                                    numChats.setText(count.toString());
                                    //Toast.makeText(PendingContracts.this,count.toString(),Toast.LENGTH_LONG).show();
                                }

                                new LongOperation().execute("");//reload the envelopes list.
                            }
                            catch (Exception e) {
                                // TODO Auto-generated catch block
                            }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 300,30000);
    }

    protected void switchPage(int newPage){




        if(newPage == 1){
            ib.setAlpha(1f);
            pic.setAlpha(0.35f);
            ch.setAlpha(0.35f);
        }else if(newPage == 2){
            ib.setAlpha(0.35f);
            pic.setAlpha(1f);
            ch.setAlpha(0.35f);
        }else if(newPage == 3){
            ib.setAlpha(0.35f);
            pic.setAlpha(0.35f);
            ch.setAlpha(1f);
        }

    }
    protected void onResume() {
        super.onResume();
        currentPage = 1;
        switchPage(1);
        //Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
        //Intercom.client().setBottomPadding(20);

        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
        new LongOperation().execute("");//reload the envelopes list.
    }

    private void initUser(){

        SharedPreferences sharedPreferences = getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
        if(sharedPreferences != null) {
            String username = sharedPreferences.getString("userName", "");
            String email = sharedPreferences.getString("userEmail", "");
            String uid = sharedPreferences.getString("userId", "");
            UserAttributes userAttributes = new UserAttributes.Builder()
                    .withName(username.replace(" ","_"))
                    .withEmail(email)
                    .build();
            //Registration registration = Registration.create().withUserId(uid);
            Registration registration = Registration.create().withEmail(email);

            Intercom.client().registerIdentifiedUser(registration);
            //Intercom.client().updateUser(userAttributes);

/*            Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);

            String uhash = getHash(email);
            Intercom.client().setUserHash(uhash);*/
        }
    }
    public String getHash(String s) {
        StringBuffer result =  new StringBuffer();
        try {
            String secret = s;
            String message = "Message";

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hash = (sha256_HMAC.doFinal(message.getBytes()));
            for (byte b : hash) {
                result.append(String.format("%02x", b));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return result.toString();
    }

    private Registration generateID(){
        Random random = new Random();
        String uid = String.valueOf(random.nextInt(500) + random.nextInt(400) + random.nextInt(1000));

        Registration res = Registration.create().withUserId(uid);

        return res;

    }


    private void successfulLogin() {
    /* For best results, use a unique user_id if you have one. */
        Registration registration = Registration.create().withUserId("123456");
        Intercom.client().registerIdentifiedUser(registration);
    }


    public void updateList(HashMap items){
        //create a new array list
        List<String> array_list = new ArrayList<String>();
        envIds = new ArrayList<String>();

        //populate the list
        Iterator myVeryOwnIterator = items.keySet().iterator();
        while(myVeryOwnIterator.hasNext()) {
            String key=(String)myVeryOwnIterator.next();
            String value=(String)items.get(key);
            array_list.add(key);
            envIds.add(value);
            //Toast.makeText(ctx, "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
        }

        //assign thelist to the adapter
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                array_list );

        //save the list for resume activity
        mainList = array_list;
        if(mainList.size() < 1){
            emptyList();
        }else{

            ImageView iv  = (ImageView) findViewById(R.id.inboxEmpty1);
            TextView tv  = (TextView) findViewById(R.id.inboxEmpty2);
            iv.setVisibility(View.INVISIBLE);
            tv.setVisibility(View.INVISIBLE);
        }

        //update the list
        lv.setAdapter(arrayAdapter);
    }


    public void emptyList(){
        ImageView iv  = (ImageView) findViewById(R.id.inboxEmpty1);
        TextView tv  = (TextView) findViewById(R.id.inboxEmpty2);
        iv.setVisibility(View.VISIBLE);
        tv.setVisibility(View.VISIBLE);

    }


    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            curl = new CurlBasic(getApplicationContext());
            String res = curl.getUser();
            if(res == null){//abort
                Intent mIntent = new Intent(PendingContracts.this, MainLogin.class);
                startActivity(mIntent);
            }else {
                //Log.d(TAG, res);
                initUser();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result != null) {
                //Log.d(TAG, result);
               /* try {
                    JSONObject jObject = new JSONObject(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
*/
                new getContracts().execute("");
            }

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    /* get the current pending contracts */
    private class getContracts extends AsyncTask<String, Void, HashMap<String,String>> {

        @Override
        protected HashMap<String, String> doInBackground(String... params) {
            curl = new CurlBasic(getApplicationContext());
                HashMap<String, String> res = curl.getContracts();
                //Log.d(TAG,res.toString());
                if(res != null) {
                return res;
            }else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(HashMap result) {
            //Log.d(TAG,result.toString());
            if(result != null) {
                updateList(result);//send the mapped data to update the list
            }else{
                //noInternet();
            }


        /*    try {
                //JSONObject jObject = new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        */

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
    private void noInternet(){
        Toast.makeText(PendingContracts.this,"No Network Access is Available.",Toast.LENGTH_LONG).show();
    }


    /* get the current pending contracts */
    private class logout extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            curl = new CurlBasic(getApplicationContext());
            Boolean res = curl.logoutUser();
            //Log.d(TAG,res.toString());
            return res;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result == true){
                Intent mIntent = new Intent(PendingContracts.this, MainLogin.class);
                startActivity(mIntent);
            }

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }


    /******** signing info (bypass the envelope screen) *******/


    private String customerName;
    private EnvelopeData envelope_data;
    private String envelopeID;

    private void signDoc(){
        new getSingingInfo().execute();
    }

    private class getSingingInfo extends AsyncTask<String, Void, HashMap<String,String>> {

        @Override
        protected HashMap<String, String> doInBackground(String... params) {
            curl = new CurlBasic(getApplicationContext());
            if(curl==null){
                return null;
            }
            HashMap<String, String> res = curl.getSigningInfo(customerName, envelopeID, envelope_data);
            //Log.d(TAG,res.toString());

            Intent mIntent = new Intent(PendingContracts.this, EmbeddedSigningActivity.class);
            mIntent.putExtra(EmbeddedSigningActivity.SIGNING_URL, res.get("url"));
            mIntent.putExtra(EmbeddedSigningActivity.RECIPIENT_EMAIL, res.get("email"));

            startActivity(mIntent);
            return res;
        }

        @Override
        protected void onPostExecute(HashMap result) {
            //Log.d(TAG,result.toString());
            if(result==null){
                noInternet();
            }

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private class loadEnvelope extends AsyncTask<String, Void, EnvelopeData> {

        @Override
        protected EnvelopeData doInBackground(String... params) {
            curl = new CurlBasic(getApplicationContext());

            if(curl==null){
                return null;
            }
            EnvelopeData res = curl.getEnvelope(envelopeID);

            return res;
        }

        @Override
        protected void onPostExecute(EnvelopeData result) {
            //Log.d(TAG,result.getRecipients().toString());
            if(result!= null) {
                envelope_data = result;
                updateCustomer(result);
                new getSingingInfo().execute();
            }else{
                noInternet();
            }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private void updateCustomer(EnvelopeData envelope){
        try {
            customerName = envelope.getEnvelope().getString("emailSubject");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}