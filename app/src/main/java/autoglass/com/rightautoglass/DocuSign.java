package autoglass.com.rightautoglass;

/**
 * Created by chrisvaughan on 3/10/18.
 */

import com.docusign.esign.api.*;
import com.docusign.esign.client.*;
import com.docusign.esign.model.*;

import java.util.List;
import java.io.IOException;

public class DocuSign {
    public static void main(String[] args) {
        String OAuthBaseUrl = "account-d.docusign.com";
        String BaseUrl = "https://demo.docusign.net/restapi";
        String RedirectURI = "";
        String IntegratorKey = "[INTEGRATOR_KEY]";
        String UserId = "[USER_ID_TO_SEND_ON_BEHALF]";
        String publicKeyFilename = "[PATH_TO_RSA265_PUBLIC_KEY]";
        String privateKeyFilename = "[PATH_TO_RSA265_PRIVATE_KEY]";

        ApiClient apiClient = new ApiClient(BaseUrl);

        try {
            // IMPORTANT NOTE:
            // the first time you ask for a JWT access token, you should grant access by making the following call
            // get DocuSign OAuth authorization url:
            //String oauthLoginUrl = apiClient.getJWTUri(IntegratorKey, RedirectURI, OAuthBaseUrl);
            // open DocuSign OAuth authorization url in the browser, login and grant access
            //Desktop.getDesktop().browse(URI.create(oauthLoginUrl));
            // END OF NOTE

            apiClient.configureJWTAuthorizationFlow(publicKeyFilename, privateKeyFilename, OAuthBaseUrl, IntegratorKey, UserId, 3600); // request for a fresh JWT token valid for 1 hour
            Configuration.setDefaultApiClient(apiClient);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            // STEP 1: AUTHENTICATE TO RETRIEVE ACCOUNTID & BASEURL
            /////////////////////////////////////////////////////////////////////////////////////////////////////////

            AuthenticationApi authApi = new AuthenticationApi();
            LoginInformation loginInfo = authApi.login();

            // parse first account ID (user might belong to multiple accounts) and baseUrl
            String accountId = loginInfo.getLoginAccounts().get(0).getAccountId();
            String baseUrl = loginInfo.getLoginAccounts().get(0).getBaseUrl();
            String[] accountDomain = baseUrl.split("/v2");

            // below code required for production, no effect in demo (same domain)
            apiClient.setBasePath(accountDomain[0]);
            Configuration.setDefaultApiClient(apiClient);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            // *** STEP 2: CREATE ENVELOPE FROM TEMPLATE
            /////////////////////////////////////////////////////////////////////////////////////////////////////////

            // create a new envelope to manage the signature request
            EnvelopeDefinition envDef = new EnvelopeDefinition();
            envDef.setEmailSubject("DocuSign Java SDK - Sample Signature Request");

            // assign template information including ID and role(s)
            envDef.setTemplateId("[TEMPLATE_ID]");

            // create a template role with a valid templateId and roleName and assign signer info
            TemplateRole tRole = new TemplateRole();
            tRole.setRoleName("[ROLE_NAME]");
            tRole.setName("[SIGNER_NAME]");
            tRole.setEmail("[SIGNER_EMAIL]");

            // create a list of template roles and add our newly created role
            java.util.List<TemplateRole> templateRolesList = new java.util.ArrayList<TemplateRole>();
            templateRolesList.add(tRole);

            // assign template role(s) to the envelope
            envDef.setTemplateRoles(templateRolesList);

            // send the envelope by setting |status| to "sent". To save as a draft set to "created"
            envDef.setStatus("sent");

            // instantiate a new EnvelopesApi object
            EnvelopesApi envelopesApi = new EnvelopesApi();

            // call the createEnvelope() API
            EnvelopeSummary envelopeSummary = envelopesApi.createEnvelope(accountId, envDef);
        } catch (ApiException ex) {
            System.out.println("Exception: " + ex);
        } catch (Exception e) {
            System.out.println("Exception: " + e.getLocalizedMessage());
        }
    }
}