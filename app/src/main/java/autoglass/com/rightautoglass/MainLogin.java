package autoglass.com.rightautoglass;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import autoglass.com.rightautoglass.utils.CurlBasic;
import io.intercom.android.sdk.Intercom;

public class MainLogin extends AppCompatActivity {
    private String User = null;
    private String Pass = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        final EditText txtUser = findViewById(R.id.txtUsername);
        final EditText txtPass = findViewById(R.id.txtPassword);


        Button btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User = txtUser.getText().toString();
                Pass = txtPass.getText().toString();

                new ProcessLogin().execute();//login
            }
        });

        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
        new checkToken().execute();
    }

    protected void onResume(){
        super.onResume();

        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE);
        new checkToken().execute();
    }

    protected void  showMessage(String s){

        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }

    private class checkToken extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            CurlBasic curl = new CurlBasic(getApplicationContext());
            Boolean res = curl.verifyToken();
            return res;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result != false){//continue to contracts page
                Intent mIntent = new Intent(MainLogin.this, PendingContracts.class);
                startActivity(mIntent);
            }else {
                //showMessage("Login Failed");
            }


        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private class ProcessLogin extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {

            CurlBasic curl = new CurlBasic(getApplicationContext());
            Boolean res = curl.loginUser(User,Pass);
            return res;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result != false){//continue to contracts page
                Intent mIntent = new Intent(MainLogin.this, PendingContracts.class);
                startActivity(mIntent);
            }else {
                showMessage("Login Failed");
            }


        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
