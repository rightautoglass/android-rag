package autoglass.com.rightautoglass;

/*import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;*/

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import autoglass.com.rightautoglass.utils.GmailMailer;

public class getPictures extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    String mCurrentPhotoPath;
    ImageView windshield;
    ImageView partNum;
    ImageView vin;
    ImageView trim;
    ImageButton sendButton;

    String strWindshield,strPartNum,strVin,strTrim;
    EditText year,make,model,txttrim;
    Drawable defautlImage;
    private ProgressDialog progressDialog;

    int requestedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_pictures);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        windshield = findViewById(R.id.imgWindshield);
        partNum = findViewById(R.id.imgPartNumber);
        vin = findViewById(R.id.imgVin);
        trim = findViewById(R.id.imgTrim);
        sendButton = findViewById(R.id.sendEmail);
        year = findViewById(R.id.txtYear);
        make = findViewById(R.id.txtMake);
        model = findViewById(R.id.txtModel);
        txttrim = findViewById(R.id.txtTrim);

        defautlImage = getResources().getDrawable(R.drawable.nophoto3x);

        windshield.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestedImage = 1;
                dispatchTakePictureIntent();
            }
        }) ;
        windshield.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(strWindshield != null) {
                    new AlertDialog.Builder(getPictures.this)
                            .setTitle("Delete Image")
                            .setMessage("Do you want to remove this image?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(getPictures.this, "Image Removed", Toast.LENGTH_SHORT).show();
                                    windshield.setImageDrawable(defautlImage);
                                    strWindshield = null;
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                }
                return false;
            }
        });

        partNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestedImage = 2;
                dispatchTakePictureIntent();
            }
        });
        partNum.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(strPartNum != null) {
                    new AlertDialog.Builder(getPictures.this)
                            .setTitle("Delete Image")
                            .setMessage("Do you want to remove this image?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(getPictures.this, "Image Removed", Toast.LENGTH_SHORT).show();
                                    partNum.setImageDrawable(defautlImage);
                                    strPartNum = null;
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                }
                return false;
            }
        });

        vin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestedImage = 3;
                dispatchTakePictureIntent();
            }
        });
        vin.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(strVin != null) {
                    new AlertDialog.Builder(getPictures.this)
                            .setTitle("Delete Image")
                            .setMessage("Do you want to remove this image?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(getPictures.this, "Image Removed", Toast.LENGTH_SHORT).show();
                                    vin.setImageDrawable(defautlImage);
                                    strVin = null;
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                }
                return false;
            }
        });

        trim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestedImage = 4;
                dispatchTakePictureIntent();
            }
        });
        trim.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(strTrim != null) {
                    new AlertDialog.Builder(getPictures.this)
                            .setTitle("Delete Image")
                            .setMessage("Do you want to remove this image?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(getPictures.this, "Image Removed", Toast.LENGTH_SHORT).show();
                                    trim.setImageDrawable(defautlImage);
                                    strTrim = null;
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                }
                return false;
            }
        });
        final Handler mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                Toast.makeText(getApplicationContext(),message.obj.toString(),Toast.LENGTH_LONG).show();

                progressDialog.dismiss();

                finish();
            }
        };

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = ProgressDialog.show(getPictures.this,"Sending data","Please wait...",false,false);
                new Thread(new Runnable() {
                    public void run() {

                        try {

                            GmailMailer sender = new GmailMailer(

                                    "pic@rightauto.glass",

                                    "Radarjr42!");


                            if(strWindshield != null){sender.addAttachment(strWindshield,"windshield.jpg");}
                            if(strPartNum != null){sender.addAttachment(strPartNum,"part_number_on_windshield.jpg");}
                            if(strVin != null){sender.addAttachment(strVin,"vin_number.jpg");}
                            if(strTrim != null){sender.addAttachment(strTrim,"trim_on_vehicle.jpg");}
                            String messageBody = "Year: " + year.getText() + "\n "
                                    + "Make: " + make.getText() + "\n "
                                    + "Model: " + model.getText() + "\n "
                                    + "Trim: " + txttrim.getText() + "\n ";

                            //get the logged in user's email
                            SharedPreferences sharedPreferences = getSharedPreferences(String.valueOf(R.string.preferences_file), MODE_PRIVATE);
                            String subject = sharedPreferences.getString("userEmail", "");

                            sender.sendMail(subject, messageBody,

                                    "pic@rightauto.glass",

                                    "pic@rightauto.glass");

                            Message message = mHandler.obtainMessage(1, "Your data has been sent.");
                            message.sendToTarget();
                        } catch (Exception e) {

                            Toast.makeText(getApplicationContext(),"Error . " + e.getMessage(),Toast.LENGTH_LONG).show();

                        } finally {


                        }

                    }

                }).start();

            }

        });

    }

    private void clearData(){
         //reset everything on the page
         Drawable defautlImage = getResources().getDrawable(R.drawable.nophoto3x);
         windshield.setImageDrawable(defautlImage);
         partNum.setImageDrawable(defautlImage);
         vin.setImageDrawable(defautlImage);
         trim.setImageDrawable(defautlImage);

         strWindshield = strPartNum = strVin = strTrim = mCurrentPhotoPath = null;

         year.setText("");
         make.setText("");
         model.setText("");
         txttrim.setText("");
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.rightautoglass.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");

            //get bitmap from file

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath,bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap,60,60,false);
            switch(requestedImage){//set the correct image preview, based on which was clicked.
                case 1: windshield.setImageBitmap(bitmap); strWindshield = mCurrentPhotoPath; break;
                case 2: partNum.setImageBitmap(bitmap); strPartNum = mCurrentPhotoPath; break;
                case 3: vin.setImageBitmap(bitmap); strVin = mCurrentPhotoPath; break;
                case 4: trim.setImageBitmap(bitmap); strTrim = mCurrentPhotoPath; break;
            }
        }
    }



    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
}
