package autoglass.com.rightautoglass;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import autoglass.com.rightautoglass.databinding.InstallerBinding;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import utlity.ImageFilePath;
import utlity.Utility;

/**
 * Created by Satnam on 1/23/2018.
 */

public class Customer extends AppCompatActivity implements View.OnClickListener {

    InstallerBinding binding;
    Toast toast;
    private static final int INITIAL_REQUEST = 1338;
    private static final int LOCATION_REQUEST = INITIAL_REQUEST + 1;

    String userChoosenTask;
    Intent mIntent;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    File destination = null;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    public static final int RequestPermissionCode = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.installer);
        binding.btnYear.setOnClickListener(this);
        binding.btnVIN.setOnClickListener(this);
        binding.btnPicture.setOnClickListener(this);
        binding.btnPaydection.setOnClickListener(this);
        binding.btnChat.setOnClickListener(this);
        EnableRuntimePermission();
        cameraPrem();

        //  successfulLogin();

        // Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE);
        //Register a user with Intercom
        Intercom.client().registerIdentifiedUser(Registration.create().withUserId("123456"));


    }

    private void cameraPrem() {
      /*  if (.checkSelfPermission(Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_REQUEST_CODE);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && Customer.this.checkSelfPermission(
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                && Customer.this.checkSelfPermission(
                android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_CAMERA_REQUEST_CODE);
        } else {
            // getLocation();
        }


    }

    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(Customer.this,
                Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(Customer.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(Customer.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Toast.makeText(Customer.this, "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(Customer.this, new String[]{
                    Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, RequestPermissionCode);

        }

    }


    private void successfulLogin() {
    /* For best results, use a unique user_id if you have one. */
        Registration registration = Registration.create().withUserId("12345sdsada6");
        Intercom.client().registerIdentifiedUser(registration);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnYear:
                toast = Toast.makeText(Customer.this, "Coming soon..", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
               // mIntent = new Intent(Customer.this, LoginActivity.class);
               // startActivity(mIntent);

                break;

            case R.id.btnVIN:
                toast = Toast.makeText(Customer.this, "Coming soon..", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                break;


            case R.id.btnPicture:
                //marshmallowCameraPremissionCheck();
                selectImage();
                break;

            case R.id.btnPaydection:
               /* toast = Toast.makeText(Customer.this, "Coming soon..", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();*/
               // mIntent = new Intent(Customer.this, CheckoutActivity.class);
               // startActivity(mIntent);

                break;


            case R.id.btnChat:
           /*     if (successfulLogin()==false) {
    *//* We're logged in, we can register the user with Intercom *//*
                    Registration registration = Registration.create().withUserId("123456");
                    Intercom.client().registerIdentifiedUser(registration);
                } else {*/
    /* Since we aren't logged in, we are an unidentified user.
     * Let's register. */
                // successfulLogin();
                Intercom.client().getUnreadConversationCount();
                Intercom.client().registerUnidentifiedUser();
                Intercom.client().displayConversationsList();
                // }
                break;
            default:
                break;

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))

                        cameraIntent();

                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }

    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {


            //binding.imageProfile.setBackgroundResource(0);
            Uri uri = data.getData();

            try {

                String mfilePath = ImageFilePath.getPath(Customer.this, data.getData());
                bm = MediaStore.Images.Media.getBitmap(Customer.this.getContentResolver(), uri);
                destination = new File(mfilePath);
                FileOutputStream fOut = new FileOutputStream(destination);
                bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
                fOut.flush();
                fOut.close();

                //binding.imageProfile.setImageBitmap(bm);

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //binding.imageProfile.setBackgroundResource(0);
        thumbnail = Bitmap.createScaledBitmap(thumbnail, 500, 500, false);

        thumbnail.compress(Bitmap.CompressFormat.PNG, 90, bytes);

        destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".png");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // mImageFile = Utility.savebitmap(thumbnail);
        // strBase64 = Utility.encodeTobase64(thumbnail);
        // binding.imageProfile.setImageBitmap(thumbnail);
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(Customer.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result1 = Utility.checkPermission(Customer.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result1 == true) {
                        cameraIntent();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result1 == true)

                    {
                        galleryIntent();
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    private void galleryIntent() {
      /*  Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);*/

        Intent intent = new Intent();

        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), SELECT_FILE);


    }

}

